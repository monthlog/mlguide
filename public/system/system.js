// Global varibles


// refs to html elements
let sidebar;
let sidebarCover;
let main;

let pd_pageDetails;
let pd_author;
let txt_author;
let lbl_author;
let img_author;
let pd_date;
let lbl_date;
let txt_date;
let pd_readingTime;
let txt_readingTime;
let txt_pageTitle;
let txt_chapterNum;
let lbl_tocTitle;

let lbl_copyright;

let logo;
let logo_icon;
let logo_title;
let logo_subTitle;

let btn_editPage;
let btn_adminPanel;
let btn_expandItems;
let btn_clearBookmarks;

let breadcrumbs;
let navigation;
let topPageData;
let tocMenu;
let topData;

let pages_menu; // TOC list reference (sidebar <ul>)
let guideContent; // documentation files will be loaded here


let PageData; // JSON object - store current page data
let homepage; // homepage url - try to read from localStorage, if not find in Pages, else use first page

let currentUrl = "";
let currentUrlID = -1;

/*// test
let url = "pages/basics/installation/docs.md";
let path = url.substring(0, url.lastIndexOf("/"));*/





// https://gitlab.com/-/ide/project/monthlog/guide/blob/master/-/public/pages/01.basics/chapter.md
//// https://gitlab.com/-/ide/project/monthlog/guide/blob/master/-/public/

function init() {
    dbg("--INIT--");

    // check if utl location is set
    if (window.location.href.includes("#")) { currentUrl = window.location.href.split("#")[1]; }


    // --- Initialize variables ---

    sidebar = document.getElementById('sidebar'); 
    sidebarCover = document.getElementById('sidebarCover'); 
    main = document.getElementById('main'); 

    logo = document.getElementById('logo'); 
    logo_icon = document.getElementById('logoIcon'); 
    logo_title = document.getElementById('logoTitle'); 
    logo_subTitle = document.getElementById('logoSubTitle'); 

    pd_pageDetails = document.getElementById('mPageDetails'); 
    pd_author = document.getElementById('pd_author'); 
    txt_author = document.getElementById('txt_author'); 
    lbl_author = document.getElementById('lbl_author'); 
    img_author = document.getElementById('img_author'); 
    pd_date = document.getElementById('pd_dateDiv');
    txt_date = document.getElementById('txt_date');  
    lbl_date = document.getElementById('lbl_date');
    pd_readingTime = document.getElementById('pd_readingTime'); 
    //lbl_readingTime = document.getElementById('lbl_readingTime');
    txt_readingTime = document.getElementById('txt_readingTime'); 
    txt_pageTitle = document.getElementById('txt_pageTitle');
    txt_chapterNum = document.getElementById('txt_chapterNum');
    lbl_tocTitle = document.getElementById('lbl_tocTitle');

    lbl_copyright = document.getElementById('lbl_copyright');

    btn_editPage = document.getElementById('btnAdminEdit');
    btn_adminPanel = document.getElementById('btnAdminPanel');
    /*btn_navPrev = document.getElementById('sidebar');
    btn_navNext = document.getElementById('sidebar');*/
    btn_expandItems = document.getElementById('btnExpandItems');
    btn_clearBookmarks = document.getElementById('btnClearBookmarks');

    pages_menu = document.getElementById('pages_menu');
    guideContent = document.getElementById('guideContent');

    breadcrumbs = document.getElementById('mBreadcrumbs');
    navigation = document.getElementById('navigation');
    topPageData = document.getElementById('topPageData');
    tocMenu = document.getElementById('tocMenu');
    topData = document.getElementById("mTopData");

    btn_editPage.style.display = 'none'; // hide page edit button - will be visible only if active and page open


    // set bookmark storage if its not set
    if (localStorage.getItem("rtfmBookmarks") === null) {
        localStorage.setItem("rtfmBookmarks", "");
    }




    loadSettings();



    loadPages();

}









function go(location) {
    if (location === "+1") {
        setTocItem(PageItem.Current - 1);
    }
    else if (location === "-1") {
        setTocItem(PageItem.Current + 1);
    }
    else if (location === "0") { // home
        setTocItem(PageItem.HomePage);
    }
    else {
        // use page url
        // page value -1 (for array index)
        // if not int - find url in array (without file extension)
    }

    dbg("Go " + location);
}

//window.onresize = function(event) { sidebar.style = ""; sidebarCover.style =""; };
window.addEventListener('resize', function(){
    sidebar.removeAttribute("style");
    sidebarCover.removeAttribute("style");
    }, true);

let isSidebarOpen = true;
function toggleSidebar(sender) {
    dbg("Toggle sidebar");

    if (typeof sender === 'undefined' || sender.id === "sidebarCover") {
        sidebar.style.marginRight = "-300px";
        sidebarCover.style.display = "none";
    }
    else if (sender.id === "btnToggleSidebar") {
        sidebar.style.marginRight = "0";
        sidebarCover.style.display = "block";
    }
}



class Helpers {

    static fileExtension(url) {
        // will return file extension es: md, html etc.
        //return url.split(/\#|\?/)[0].split('.').pop().trim();
        return url.split('.').pop();
    }

    static fullfileName(url)  {
        // will return full file name with extension, ex: file.html
        return url.substring(url.lastIndexOf('/') + 1);
    }

    static navigationUrl(url) {
        return url.substring(0, url.lastIndexOf('.')) || url;
    }

    static fileDirectory(url) {
        return url.split("/").slice(0,-1).join("/")
    }

    static formatDate(longDate) {
        let dt = new Date(longDate);

        return ((dt.getDate() < 10) ?
            ("0" + dt.getDate()) : dt.getDate()) + "." + (((dt.getMonth() + 1) < 10) ? ("0" + (dt.getMonth() + 1)) :
            (dt.getMonth() + 1)) + "." + dt.getFullYear();
    }

    static readingTime(text) {
        // allow sending preset words count
        let words = ((/\s/.test(text)) || (isNaN(text)) ? text.split(/\s+/).length : parseInt(text)) ;

        // calculate reading time
        let rt = words / Settings.words_per_minute;

        let m = Math.floor(rt); // minutes
        let s = Math.floor(((rt % 1) * 0.60) * 100); // seconds

        if (s < 10) { s = ("0" + s) ;} // add 0 to seconds value

        let message;

        if (m < 1 && s <= 10) { message = "10 " + Lang.time_seconds; }
        else if (m < 1 && s > 10) { message = s + " " + Lang.time_seconds; }
        else if (m === 1 && s <= 10) { message = Lang.time_minute; }
        else if (m > 1 && s <= 10) { message = m + " " + Lang.time_minutes; }
        else if (m > 1 && s > 10) { message = m + ":" + s + " " + Lang.time_minutes; }

        // set bookmark timer
        if (Settings.use_bookmarks) {
            clearTimeout(bookmarkTimer); // stop previous timer if its running

            let delay = m * 60000 + s * 1000; // set delay time based on reading time

            // shorten delay time
            /*if (m > 0) { delay -= (m * 30000); }
            if (s > 0) { delay -= (s * 300); }*/
            if (delay > 0) { delay /= 2; } // 1/2 delay time

            bookmarkTimer = setTimeout(setBookmarkTimeout, delay);
        }

        return message + " " + Lang.page_reading_time;
    }

    static isHttp(url) {

        return (url.trim().startsWith("http"));

        //return (url.startsWith("http://") || url.startsWith("https://"))

       // return (!/^(f|ht)tps?:\/\//i.test(url));

       /* if (!/^(f|ht)tps?:\/\//i.test(url)) {
            url = "http://" + url;
        }
        return url;*/
    }

    static setBodyClass(pageType) {
        document.body.dataset.type = "";
        if ((typeof pageType !== 'undefined')) { document.body.dataset.type = pageType; }

        //document.body.className = "";
        //document.body.removeAttribute("class");

        //if ((typeof className !== 'undefined')) { document.body.classList.add(className); }
    }

}



function dbg(data) {

    if (!Settings.debug_mode_active ||
        localStorage.getItem("rtfmDebug") === null ||
        localStorage.getItem("rtfmDebug") === "false") { return; }

    console.log("--[Debug]  " + data);
}



function openDocFile(url) {
    doGETpage("pages/" + url).then(function(fileData)
    {
        dbg("--Open File--");
        dbg("--pages/" + url);

        let split = fileData.split(Settings.split_by);

        if (split.length !== 3) { dbg("Page meta not found!"); throw "error"; }


        PageData = JSON.parse(split[1]); // get page data json

        // page tab title and add page name to it if setting is set
        if (Settings.show_page_title_inTab) {
            document.title = Settings.page_tab_title + Settings.show_page_title_spacer + PageData.title;
        }

        // set page data
        txt_author.innerText = PageData.writer;
        img_author.src = "images/user_" + PageData.writer + ".png";

        txt_pageTitle.innerText = PageData.title;

        // if update date set in PageData use it, if not get date from request header
        txt_date.innerText = ((PageData.update !== "") ? PageData.update : split[0]);


        // reading time
        txt_readingTime.innerText = Helpers.readingTime(split[2], Settings.words_per_minute);


        main.scrollTop = 0;

        // set style for each file type
        let fileType = Helpers.fileExtension(url).toLowerCase();
        guideContent.dataset.type = fileType;

        guideContent.dataset.direction = PageData.direction;

        split[2] = split[2].trim(); // remove extra white space

        // set content md / html / txt files
        if (fileType === "md") {
            guideContent.innerHTML = marked(split[2]);
        }
        else if (fileType === "html") {
            guideContent.innerHTML = split[2]; //new DOMParser().parseFromString(split[2], "text/html");
        }
        else {
            guideContent.innerHTML = split[2].replace(/(?:\r\n|\r|\n)/g, '<br>'); // replace new lines with <br>
        }

        // set additional content style for each page template
        guideContent.dataset.template = PageData.template;

        // hide page details if set off in settings or if chapter page
        if (Settings.show_page_info) {
            if (PageData.template === "chapter") {
                topData.classList.add("hideDetails");
            }
            else {
                topData.classList.remove("hideDetails");
            }
        }


        // fix images sources
        let images = document.getElementById("guideContent").getElementsByTagName("img");
        if (images.length > 0) {

            let imagePath = "pages/" + Helpers.fileDirectory(url) + "/";
            for (let i = 0; i< images.length; i++) {
                if (images[i].src.startsWith(document.location.origin)) {
                    images[i].src = imagePath + Helpers.fullfileName(images[i].src);

                    //images[i].src = imagePath + images[i].src.substring(images[i].src.lastIndexOf("/"), images[i].src.length);
                }
            }
        }


        // page edit link
        btn_editPage.style.display = ((!Settings.show_page_edit_link || localStorage.getItem("rtfmsiteauthor") === null) ? 'none' : 'block' );
        btn_editPage.href = Settings.page_edit_url + url;

        // admin panel link button
        btn_adminPanel.style.display = ((!Settings.admin_panel_active || localStorage.getItem("rtfmsiteauthor") === null) ? 'none' : 'block' );


        setBreadcrumbs();


        // prev / next navigation
        if (!Settings.show_navigation || PageItem.Current === 0) { // first page
            navigation.childNodes[0].style.display = "none";
        }
        else {
            navigation.childNodes[0].removeAttribute("style");
        }

        // last page
        if (!Settings.show_navigation ||
            PageItem.Current === PageItemsArr.length - 1 ||
            (PageItem.Current < PageItemsArr.length - 1 && PageItemsArr[PageItem.Current+1].level === 0)) {
            navigation.childNodes[1].style.display = "none";
        }
        else {
            navigation.childNodes[1].removeAttribute("style");
        }

        // set current page in localStorage
        localStorage.setItem("rtfmPage", PageItem.Current);

        // change page style depending on page content and type
        Helpers.setBodyClass(PageData.template);

    })
    .catch(function(xhr) {
        // The call failed, look at `xhr` for details

        // show error 404 page
        Helpers.setBodyClass("error");

        txt_pageTitle.innerText = Lang.error_message_title;
        guideContent.innerHTML = Lang.error_404_message;

        navigation.childNodes[0].style.display = "none";
        navigation.childNodes[1].style.display = "none";

        _setCurrentItem();

        /*for (let i = 0; i < PageItemsArr.length; i++) {
            if (PageItemsArr[i].html.classList.contains("current")) {
                PageItemsArr[i].html.classList.remove("current");
            }
        }

        PageItem.Current = -1;*/

    });

    setTimeout( () => {
        main.style.transition = "opacity .4s ease-in";
        main.style.opacity = "1";
    }, 100);
}



function loadSettings() {
    dbg("--Load Settings--");


    if (localStorage.getItem("rtfmLang") === null) {
        localStorage.setItem("rtfmLang", Settings.site_language_code);
        localStorage.setItem("rtfmDir", Settings.site_direction);
    }

    // logo
    logo_icon.src = Settings.site_logo;
    logo_title.innerText = Settings.site_title;
    logo_subTitle.innerText = Settings.site_subTitle;

    // page tab title *
    document.title = Settings.page_tab_title + " - " + Settings.site_subTitle;

    // reading time
    if (Settings.show_page_info) {
        pd_readingTime.style.display = (!Settings.show_reading_time) ? 'none' : 'block';
    }

    // site html attributes
    document.documentElement.lang = Settings.site_language_code;
    document.documentElement.dir = Settings.site_direction;

    btn_clearBookmarks.title = Lang.bookmarks_clear;
    btn_adminPanel.title = Lang.admin_panel;

    lbl_date.innerText = Lang.page_last_update;
    lbl_author.innerText = Lang.page_author;

    lbl_tocTitle.innerText = Lang.toc_title;


    if (!Settings.show_navigation) {
        sidebar.remove();
        navigation.remove();
        main.style.marginRight = "0!important";
    }


    lbl_copyright.innerHTML = (Settings.show_copyright) ? Lang.site_copyright : null;
    /*if (!Settings.show_copyright) {
        lbl_copyright.style.display = "none";
    }
    else {
        lbl_copyright.innerHTML = Lang.site_copyright;
    }*/


    // show ease-in effect on page load
    setTimeout( () => {
        document.body.style.transition = "opacity .4s ease-in";
        document.body.style.opacity = "1";
    }, 300);

}




let PageItemsArr = []; // store all menu items


function _setCurrentItem(itemID = -1) {
    // nothing selected
    if (itemID < 0) {
        if (PageItem.Current > -1) {
            PageItemsArr[PageItem.Current].html.classList.remove("current");

            PageItem.Current = -1;
        }
        else {
            for (let i = 0; i < PageItemsArr.length; i++) {
                if (PageItemsArr[i].html.classList.contains("current")) {
                    PageItemsArr[i].html.classList.remove("current");
                }
            }
        }

        return;
    }

    if (itemID !== PageItem.Current) {
        if (PageItem.Current > -1) {
            PageItemsArr[PageItem.Current].html.classList.remove("current");
        }

        PageItem.Current = itemID;

        PageItemsArr[PageItem.Current].html.classList.add("current");
    }

}


function setTocItem(itemID) {

    // check if out of array or same item
    if (itemID < 0 || itemID >= PageItemsArr.length - 1 || itemID === PageItem.Current) { return; }

    if (window.innerWidth < 801) { toggleSidebar(); }


    let item = PageItemsArr[itemID].html; // get item html ref

    // hide content to create ease-in effect on load
    main.style.transition = "none";
    main.style.opacity = "0";

    item.dataset.show = "1";

    // todo
    // - set bookmark
    // - store in local data
    // -- will be done in Helpers.readingTime

    _setCurrentItem(itemID);

    expandItemInList(); // expand toc list menu if needed


    // show chapter number for each chapter
    if (item.dataset.chapter > 0) {
        txt_chapterNum.innerText = Lang.page_chapter_num + " " + item.dataset.chapter;
        txt_chapterNum.style.display = 'block';
    }
    else {
        txt_chapterNum.innerText = null;
        txt_chapterNum.style.display = 'none';
    }

    // load file
    openDocFile(PageItemsArr[PageItem.Current].url);
}


let bookmarkTimer; // store timer
function setBookmarkTimeout() {
    PageItemsArr[PageItem.Current].html.dataset.bookmark = "1";

    // todo - add localstorage bookmark items
}


function setBreadcrumbs() {
    breadcrumbs.innerHTML = null; // remove previous data

    let levels = PageItemsArr[PageItem.Current].level;
    let element;

    for (let i = PageItem.Current; i >= 0; i--) {
        if (PageItemsArr[i].level < levels) {
            element = document.createElement("a");
            element.innerText = PageItemsArr[i].title;
            element.setAttribute("href", "#" + PageItemsArr[i].url);
            element.setAttribute("onclick", "setTocItem(" + PageItemsArr[i].html.dataset.id + ")");

            breadcrumbs.prepend(element);
            levels--;
        }
    }

    // add current item title as span
    element = document.createElement("span");
    element.innerText = PageItemsArr[PageItem.Current].title;

    breadcrumbs.appendChild(element); // add current page
}


// todo - save side bar toc expand state and set it on page load
let isExpanded = false; // store sidebar visibility state (open/ close)
function toggleItemsView(sender) {
    isExpanded = !isExpanded;

    // update localStorage value only if button was pressed
    if (typeof sender !== 'undefined') { localStorage.setItem("rtfmExpanded", isExpanded); }


    if (isExpanded) {
        btn_expandItems.classList.remove("fa-plus-square"); // button icon
        btn_expandItems.classList.add("fa-minus-square");

        tocMenu.classList.add("expanded"); // toc list background color

        for (let i = 0; i < PageItemsArr.length; i ++) {
            PageItemsArr[i].html.dataset.show = "1";
        }
    }
    else {
        btn_expandItems.classList.remove("fa-minus-square"); // button icon
        btn_expandItems.classList.add("fa-plus-square");

        tocMenu.classList.remove("expanded");

        // no selected item
        if (PageItem.Current < 0) {
            for (let i = 0; i < PageItemsArr.length; i ++) {
                PageItemsArr[i].html.dataset.show = "0";
            }
        }
        else { expandItemInList(); }
    }

}



function expandItemInList() {
    if (isExpanded === false && PageItem.Current > -1) {
        let i;
        let expand = "1";

        if (PageItemsArr[PageItem.Current].html.dataset.level === "1") {
            for (i = 0; i < PageItemsArr.length; i++) {
                if (i < PageItem.Current) {
                    PageItemsArr[i].html.dataset.show = "0";
                }
                else if (i === PageItem.Current) {
                    PageItemsArr[i].html.dataset.show = expand;
                }
                else {
                    if (expand === "1" && PageItemsArr[i].html.dataset.level < 2) {
                        expand = "0";
                    }

                    PageItemsArr[i].html.dataset.show = expand;
                }
            }
        }
        else { // not chapter item
            // go up from current item pos
            for (i = PageItem.Current - 1; i >= 0; i--) {
                PageItemsArr[i].html.dataset.show = expand;

                if (expand === "1" && PageItemsArr[i].html.dataset.level < 2) { expand = "0"; }
            }

            expand = "1"; // reset flag

            // go down from current item pos
            for (i = PageItem.Current + 1; i < PageItemsArr.length; i++) {
                if (expand === "1" && PageItemsArr[i].html.dataset.level < 2) { expand = "0"; }

                PageItemsArr[i].html.dataset.show = expand;
            }

            PageItemsArr[PageItem.Current].html.dataset.show = "1"; // show current item
        }
    }
}



function clearBookmarks() {
    if (PageItemsArr.length > 0) {
        for (let i = 0; i < PageItemsArr.length; i++) {
            PageItemsArr[i].html.dataset.bookmark = "0";
        }
    }

    //localStorage.setItem("rtfmBookmarks", "");
    localStorage.removeItem("rtfmPage");
    localStorage.removeItem("rtfmBookmarks");
}



function adminPanel() {
    if (Settings.admin_panel_active === false) { return; }

    window.location.replace("admin.html");
}



function loadPages() {
    dbg("--Load Pages--");

    let pArr = Pages.trim().split('\n'); // array of all pages

    // for each line in pages.js, create toc menu item and add it to TocMenu in sidebar
    for (let i = 0; i < pArr.length; i++) {
        new PageItem(pArr[i]);
    }

    // expand toc if needed
    if (localStorage.getItem("rtfmExpanded") !== null && localStorage.getItem("rtfmExpanded") === "true") {
        toggleItemsView(); // will set isExpanded to true
    }


    // set current page:
    // 1. if url location is set, try to open it
    // 2. else if localStorage rtfmLastPgae is set try to load it
    // 3. else open homepage

    if (currentUrlID > -1) { setTocItem(currentUrlID); }
    else if (localStorage.getItem("rtfmPage") !== null && parseInt(localStorage.getItem("rtfmPage")) > -1) {
        setTocItem(parseInt(localStorage.getItem("rtfmPage")));
    }
    else { setTocItem(PageItem.HomePage); }

}



class PageItem {
    constructor(line)
    {
        let p = line.split("¦"); // 0: >>, 1: url, 2: title, 3: isHomePage (?)

        if (p.length < 3) { return; }

        this.html = document.createElement("a");

        this.url = p[1];

        // check if list item is URL
        if (p[1].startsWith("http")) {
            this.type = "url";
            this.level = 0;
            this.html.setAttribute("href", this.url);
            this.html.setAttribute("target", "_blank");
        }
        else {
            this.type = p[1].split('.').pop();  // file type: md, html, txt

            // get page level
            p[0] = p[0].trim();
            if (p[0].includes("#")) {
                PageItem.HomePage = PageItem.Total; // set home page
                p[0] = p[0].replace(/#/g,"");
            }

            this.level = p[0].length;

            _itemHrefTemp = Helpers.navigationUrl(this.url);

            this.html.setAttribute("href", "#" + _itemHrefTemp);
            //this.html.setAttribute("href", "#" + Helpers.navigationUrl(this.url));
            this.html.setAttribute("onclick", "setTocItem(" + PageItem.Total + ")");

            // set page id if give url exists
            if (_itemHrefTemp === currentUrl) { currentUrlID = PageItem.Total; }
        }


        this.title = p[2]; // page title (display)

        this.html.dataset.level = this.level;
        this.html.dataset.show = "0";
        this.html.dataset.id = PageItem.Total;

        // add chapter number to chapter menu item
        if (this.level < 2)
        {
            this.html.dataset.chapter = (++PageItem.Chapters).toString();
            this.chapter = PageItem.Chapters;
        }

        this.html.innerText = this.title;
        this.html.dataset.bookmark = "0"; // 0: none, 1: bookmarked, 2: bookmarked but page was updated since

        tocMenu.appendChild(this.html); // add to page toc menu

        PageItemsArr.push(this); // add to array

        PageItem.Total++; // count instances
    }
}

/* static variables */
PageItem.Total = 0; // total items counter used for items ids
PageItem.Current = -1; // current item id
PageItem.HomePage = 0; // home page item id
PageItem.Chapters = 0; // used for chapter enumeration

let _itemHrefTemp;




// used to send XMLHttpRequest to the server
function doGETpage(path, callback) {
    return new Promise(function(resolve, reject) {
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function() {
            if (xhr.readyState == 4) {
                // The request is done; did it work?
                if (xhr.status == 200) {
                    // Yes, use `xhr.responseText` to resolve the promise
                    resolve(Helpers.formatDate(xhr.getResponseHeader("Last-Modified")) + "≡" + xhr.responseText);
                    //resolve(xhr.responseText);
                } else {
                    // No, reject the promise
                    reject(xhr);
                }
            }
        };
        xhr.open("GET", path, true);
        xhr.send();
    });
}

