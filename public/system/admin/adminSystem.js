let ap_panels; // 0. ap_c_pages, 1. ap_c_settings, 2. ap_c_language, 3. ap_c_result
let ap_menuItems; // 0. ap_m_pages, 1. ap_m_settings, 2. ap_m_lang
let ap_resultCodeText; // text area
let ap_btnShowSource;
let ap_currentMenu = 0;


function ap_init() {
    // initialize refs
    ap_panels = [
        document.getElementById("ap_c_pages"),
        document.getElementById("ap_c_settings"),
        document.getElementById("ap_c_language"),
        document.getElementById("ap_c_create"),
        document.getElementById("ap_c_result")
    ];

    ap_menuItems = [
        document.getElementById("ap_m_pages"),
        document.getElementById("ap_m_settings"),
        document.getElementById("ap_m_lang"),
        document.getElementById("ap_m_create")
    ];

    ap_resultCodeText = document.getElementById("ap_code_result_text");

    ap_btnShowSource = document.getElementById("ap_btnShowSource");
}


function ap_showCodeResult() {

    // show source code
    if (ap_btnShowSource.dataset.show === "1")
    {
        ap_btnShowSource.dataset.show = "0";
        ap_btnShowSource.innerText = "Hide";

        ap_panels[ap_currentMenu].className = "hide";
        ap_panels[ap_panels.length-1].className = "";
    }
    else {
        ap_btnShowSource.innerText = "Show";
        ap_btnShowSource.dataset.show = "1";

        ap_panels[ap_currentMenu].className = "";
        ap_panels[ap_panels.length-1].className = "hide";
    }
}


function ap_copyCodeToClipboard() {

    let hideclass = "";

    if (ap_panels[ap_panels.length-1].classList.contains("hide")){
        hideclass = "hide";
        ap_panels[ap_panels.length-1].className = "";
    }

    ap_resultCodeText.select();
    document.execCommand("copy");

    ap_panels[ap_panels.length-1].className = hideclass;

    document.getSelection().removeAllRanges();
    document.activeElement.blur();
}


function ap_menuItem(button) {
    ap_currentMenu = parseInt(button.dataset.apid);

    for (let i = 0; i < ap_menuItems.length; i++) {
        ap_menuItems[i].className = ((ap_currentMenu === i) ? "ap_menu_current" : "");
        ap_panels[i].className = ((ap_currentMenu !== i) ? "hide" : "");
    }

    ap_panels[ap_panels.length-1].className = "hide";

    ap_btnShowSource.innerText = "Show";
    ap_btnShowSource.dataset.show = "1";
}


function ap_saveFile() {
    // todo: add text generator for settings or pageslet

    let text = $("#ap_code_result_text").val();
    let filename = "settings";
    let blob = new Blob([text], {type: "application/javascript;charset=utf-8"});
    saveAs(blob, filename + ".js");

}



/*
$("#btn-save").click( function() {
    var text = $("#textarea").val();
    var filename = $("#input-fileName").val()
    var blob = new Blob([text], {type: "text/plain;charset=utf-8"});
    saveAs(blob, filename+".txt");
});
*/

/*
$("#btn-save").click( function() {
    var text = $("#textarea").val();
    var filename = $("#input-fileName").val()
    var blob = new Blob([text], {type: "text/plain;charset=utf-8"});
    saveAs(blob, filename+".txt");
});*/






/*  Sortable list  */


//window.onload = ap_init();




$(document).ready(function() {

    ap_init();

    var updateOutput = function(e)
    {
        console.log("Page item reordered "); // + e.target.innerHTML
    };

    // activate Nestable for list 1
    $('#nestable').nestable( {group: 1} ).on('change', updateOutput);

    // activate Nestable for list 2
    //$('#nestable2').nestable( {group: 1} ).on('change', updateOutput);

    // output initial serialised data
    //updateOutput($('#nestable').data('output', $('#nestable-output')));
    //updateOutput($('#nestable2').data('output', $('#nestable2-output')));

    // expand buttons
    $('#nestable-menu').on('click', function(e) {
        var target = $(e.target), action = target.data('action');

        if (action === 'expand-all') { $('.dd').nestable('expandAll'); }

        if (action === 'collapse-all') { $('.dd').nestable('collapseAll'); }
    });

   // $('#nestable3').nestable();

});