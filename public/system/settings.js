class Settings {}
class Lang {}

Settings.site_logo = 'images/logo.png';
Settings.site_title = 'מעקב לקוחות';
Settings.site_subTitle = 'מדריך למשתמש';
Settings.page_tab_title = 'מעקב לקוחות';
Settings.show_page_title_inTab = true;
Settings.show_page_title_spacer = ' - ';
Settings.show_page_edit_link = true;
Settings.page_edit_url = 'https://gitlab.com/-/ide/project/monthlog/guide/blob/master/-/public/pages/';
Settings.show_page_info = true;
Settings.show_copyright = true;
Settings.show_reading_time = true;
Settings.show_breadcrumbs = false;
Settings.show_navigation = false;
Settings.use_bookmarks = true;
Settings.admin_panel_active = true;
Settings.debug_mode_active = true;
Settings.words_per_minute = 90;
Settings.site_language_code = 'he';
Settings.site_direction = 'rtl';
Settings.images_root_dir = '';
Settings.split_by = '≡';


Lang.page_author = 'נכתב ע"י';
Lang.page_post_date = 'פורסם בתאריך';
Lang.toc_title = 'תוכן עניינים';
Lang.toc_expand = 'פתח תפריטים';
Lang.toc_collapse = 'קווץ תפריטים';
Lang.bookmarks_clear = 'נקה סימניות';
Lang.page_last_update = 'עודכן בתאריך';
Lang.edit_this_page = 'ערוך עמוד זה';
Lang.admin_panel = 'כלי עזר למנהל';
Lang.error_message_title = 'התרחשה שגיאה!';
Lang.error_404_message = '<h6>404<br>העמוד המבוקש לא נמצא...</h6><br><a href="#" onclick="go(\'0\')">חזור לעמוד ראשי</a>';
Lang.page_chapter_num = 'פרק';
Lang.page_reading_time = 'קריאה';
Lang.time_second = 'שניה';
Lang.time_seconds = 'שניות';
Lang.time_minute = 'דקה';
Lang.time_minutes = 'דקות';
Lang.time_one_minute = 'דקה אחת';
Lang.site_copyright =
    'כל הזכויות שמורות' +
    '<i class="fas fa-copyright" style="padding:0 4px; font-weight:400; font-size:0.7rem;"></i>' +
    '<span style="font-size:0.7rem;">' + (new Date()).getFullYear() +' </span>'
;