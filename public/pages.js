// ------- INFO -------
// - Edit data inside ` ` only
// - Add each page on new line
// - > used to set pages structure, each additional > used for setting sub page
// - Tab indentations are optional
// - Directory name should not begin with /
// - Directories is relative to [pages/] folder
// - File extension is required
// - Add # to the page that should be set as home page, if not set, first page will be used
// - To add external url make sure it starts with http:// or https://

// Format:
// item level ¦ url ¦ page title ¦ * last edit date

// Example 1:
// >#¦features/features.html¦Features

// Example 2:
/*
>#¦features/features.html¦מאפייני התוכנה
>¦basics/chapter.md¦מבוא
    >>¦basics/installation/docs.md¦הוראות התקנה
    >>¦basics/overview/docs.md¦כללי
    >>¦intermediate/doc5.html¦ביצוע דוחות מעקב
    >>¦basics/requirements/docs.md¦Requirements
        >>>¦basics/requirements/docs.md¦דרישות מערכת - מינ
>¦https://www.google.com¦Web Site
*/


var Pages = `
>#¦features/features.html¦מאפייני התוכנה
>¦basics/chapter.md¦מבוא
    >>¦basics/installation/docs.md¦הוראות התקנה
    >>¦basics/overview/docs.md¦כללי
    >>¦intermediate/doc5.html¦ביצוע דוחות מעקב
    >>¦basics/requirements/docs.md¦דרישות מערכת
        >>>¦basics/requirements/docs.md¦דרישות מערכת - מינימום
        >>>¦intermediate/text_doc1.txt¦אודות Grav
>¦coop/chapter.md¦שיתוף פעולה
    >>¦features/docs.md¦מאפייני התוכנה
    >>¦installation/docs.html¦הוראות התקנה
>¦.print/docs.md¦הדפסת דוחות
    >>¦printer-setup.md¦הגדרות מדפסת
    >>¦print.md¦ביצוע דוחות
>¦basics/installation/docs.md¦שאלות נפוצות
>¦http://monthlog.heliohost.org/guide/¦קישור לאתר
`;